use warp::{http::Method, Filter, Rejection};

pub fn lock() -> impl Filter<Extract = ((),), Error = Rejection> + Copy {
    warp::method().and_then(|method: Method| async move {
        if method.as_str() == "LOCK" {
            Ok(())
        } else {
            Err(warp::reject())
        }
    })
}

pub fn unlock() -> impl Filter<Extract = ((),), Error = Rejection> + Copy {
    warp::method().and_then(|method: Method| async move {
        if method.as_str() == "UNLOCK" {
            Ok(())
        } else {
            Err(warp::reject())
        }
    })
}
