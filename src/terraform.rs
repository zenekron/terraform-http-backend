use chrono::prelude::*;
use serde::{Deserialize, Serialize};

/// LockInfo stores lock metadata.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct LockInfo {
    /// Unique ID for the lock.
    #[serde(rename = "ID")]
    pub id: String,

    /// Terraform operation, provided by the caller.
    #[serde(rename = "Operation")]
    pub operation: String,

    /// Extra information to store with the lock, provided by the caller.
    #[serde(rename = "Info")]
    pub info: String,

    /// user@hostname when available.
    #[serde(rename = "Who")]
    pub who: String,

    /// Terraform version.
    #[serde(rename = "Version")]
    pub version: String,

    /// Time that the lock was taken.
    #[serde(rename = "Created")]
    pub created: DateTime<Local>,

    /// Path to the state file when applicable. Set by the Lock implementation.
    #[serde(rename = "Path")]
    pub path: String,
}
