use crate::error::Error;
use std::{fs, path::Path};
use warp::{http::Response, hyper::body::Bytes, Reply};

pub struct State(Vec<u8>);

impl State {
    pub fn from_file(path: impl AsRef<Path>) -> Result<Self, Error> {
        Ok(State(fs::read(path)?))
    }

    pub fn to_file(&self, path: impl AsRef<Path>) -> Result<(), Error> {
        Ok(fs::write(path, self)?)
    }
}

impl AsRef<[u8]> for State {
    fn as_ref(&self) -> &[u8] {
        &self.0[..]
    }
}

impl From<Vec<u8>> for State {
    fn from(value: Vec<u8>) -> Self {
        State(value)
    }
}

impl From<Bytes> for State {
    fn from(value: Bytes) -> Self {
        value.as_ref().to_vec().into()
    }
}

impl Reply for State {
    fn into_response(self) -> warp::reply::Response {
        Response::builder().status(200).body(self.0.into()).unwrap()
    }
}
