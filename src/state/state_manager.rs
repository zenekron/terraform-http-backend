use crate::{error::Error, state::State, terraform::LockInfo};
use hashbrown::HashMap;
use std::{
    fs, io,
    path::{Path, PathBuf},
    sync::{Arc, Mutex},
};

pub trait StateManager<State> {
    type Error;

    fn fetch(&self, name: impl AsRef<str>) -> Result<State, Self::Error>;
    fn update(&self, name: impl AsRef<str>, data: impl Into<State>) -> Result<(), Self::Error>;
    fn purge(&self, name: impl AsRef<str>) -> Result<(), Self::Error>;
}

pub trait StateManagerWithLocking<State>: StateManager<State> {
    fn update_locked(
        &self,
        name: impl AsRef<str>,
        data: impl Into<State>,
        lock_id: impl AsRef<str>,
    ) -> Result<(), Self::Error>;
    fn lock(&self, name: impl AsRef<str>, lock_info: LockInfo) -> Result<LockInfo, Self::Error>;
    fn unlock(&self, name: impl AsRef<str>, lock_info: LockInfo) -> Result<(), Self::Error>;
}

pub struct FilesystemStateManager {
    basedir: String,
    locks: Arc<Mutex<HashMap<String, LockInfo>>>,
}

impl FilesystemStateManager {
    pub fn new(basedir: impl AsRef<str>) -> Result<Self, Error> {
        let basedir = basedir.as_ref();

        {
            let basedir = Path::new(basedir);
            if basedir.exists() {
                if !basedir.is_dir() {
                    return Err(Error::IoError(io::Error::new(
                        io::ErrorKind::Other,
                        "States directory is not a folder",
                    )));
                }
            } else {
                fs::create_dir_all(basedir)?;
            }
        }

        Ok(FilesystemStateManager {
            basedir: basedir.to_owned(),
            locks: Arc::new(Mutex::new(HashMap::new())),
        })
    }

    fn state_file(&self, name: &str) -> PathBuf {
        [&self.basedir, name].iter().collect()
    }
}

impl StateManager<State> for FilesystemStateManager {
    type Error = Error;

    fn fetch(&self, name: impl AsRef<str>) -> Result<State, Self::Error> {
        let name = name.as_ref();
        let state_file = self.state_file(name);

        if state_file.exists() {
            Ok(State::from_file(state_file)?.into())
        } else {
            Err(Error::NotFound(name.to_owned()))
        }
    }

    fn update(&self, name: impl AsRef<str>, data: impl Into<State>) -> Result<(), Self::Error> {
        let name = name.as_ref();
        let state_file = self.state_file(name);
        let locks = self.locks.lock().unwrap();

        if locks.contains_key(name) {
            Err(Error::Locked(name.to_owned()))
        } else {
            Ok(data.into().to_file(state_file)?)
        }
    }

    fn purge(&self, name: impl AsRef<str>) -> Result<(), Self::Error> {
        let name = name.as_ref();
        let state_file = self.state_file(name);
        let locks = self.locks.lock().unwrap();

        if locks.contains_key(name) {
            Err(Error::Locked(name.to_owned()))
        } else if state_file.exists() {
            Ok(fs::remove_file(state_file)?)
        } else {
            Err(Error::NotFound(name.to_owned()))
        }
    }
}

impl StateManagerWithLocking<State> for FilesystemStateManager {
    fn update_locked(
        &self,
        name: impl AsRef<str>,
        data: impl Into<State>,
        lock_id: impl AsRef<str>,
    ) -> Result<(), Self::Error> {
        let name = name.as_ref();
        let state_file = self.state_file(name);
        let locks = self.locks.lock().unwrap();

        match locks.get(name) {
            Some(local) if local.id == lock_id.as_ref() => Ok(data.into().to_file(state_file)?),
            _ => Err(Error::InvalidLock(
                name.to_owned(),
                lock_id.as_ref().to_owned(),
            )),
        }
    }

    fn lock(&self, name: impl AsRef<str>, lock_info: LockInfo) -> Result<LockInfo, Self::Error> {
        let name = name.as_ref();
        let mut locks = self.locks.lock().unwrap();

        match locks.get(name) {
            Some(local) if *local == lock_info => Ok(lock_info),
            Some(_) => Err(Error::InvalidLock(name.to_owned(), lock_info.id.clone())),
            None => {
                locks.insert(name.to_owned(), lock_info.clone());
                Ok(lock_info)
            }
        }
    }

    fn unlock(&self, name: impl AsRef<str>, lock_info: LockInfo) -> Result<(), Self::Error> {
        let name = name.as_ref();
        let mut locks = self.locks.lock().unwrap();

        match locks.get(name) {
            Some(local) if *local == lock_info => {
                locks.remove(name);
                Ok(())
            }
            _ => Err(Error::InvalidLock(name.to_owned(), lock_info.id.clone())),
        }
    }
}
