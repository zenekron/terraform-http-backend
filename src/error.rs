use std::io;
use thiserror::Error as ThisError;
use warp::{
    http::{Response, StatusCode},
    hyper::Body,
    Reply,
};

#[derive(Debug, ThisError)]
pub enum Error {
    #[error(transparent)]
    IoError(#[from] io::Error),

    #[error(transparent)]
    JsonError(#[from] serde_json::Error),

    #[error("Could not find state {0}")]
    NotFound(String),

    #[error("State {0} is locked")]
    Locked(String),

    #[error("Invalid lock {1} for state {0}")]
    InvalidLock(String, String),
}

impl Into<StatusCode> for Error {
    fn into(self) -> StatusCode {
        match self {
            Error::IoError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Error::JsonError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            Error::NotFound(_) => StatusCode::NOT_FOUND,
            Error::Locked(_) => StatusCode::LOCKED,
            Error::InvalidLock(_, _) => StatusCode::CONFLICT,
        }
    }
}

impl Reply for Error {
    fn into_response(self) -> warp::reply::Response {
        match &self {
            Error::IoError(err) => {
                eprintln!("{}", err);
            }
            Error::JsonError(err) => {
                eprintln!("{}", err);
            }
            _ => {}
        };

        let status_code: StatusCode = self.into();

        Response::builder()
            .status(status_code)
            .body(match status_code.canonical_reason() {
                Some(reason) => reason.into(),
                None => Body::empty(),
            })
            .unwrap()
    }
}
