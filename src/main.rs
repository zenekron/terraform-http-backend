#[macro_use]
extern crate lazy_static;

mod error;
mod filters;
mod state;
mod terraform;

use crate::{
    error::Error,
    state::{FilesystemStateManager, StateManager, StateManagerWithLocking},
    terraform::LockInfo,
};
use serde::Deserialize;
use std::{env, net::SocketAddr};
use warp::{
    http::Response,
    hyper::{body::Bytes, Body},
    Filter, Reply,
};

const ADDRESS_DEFAULT: &str = "127.0.0.1:8080";
const STATES_DIR_DEFAULT: &str = "./states";

const ADDRESS_VAR: &str = "BACKEND_ADDRESS";
const STATES_DIR_VAR: &str = "BACKEND_STATES_DIR";

lazy_static! {
    pub static ref STATES: FilesystemStateManager = {
        let basedir = env::var(STATES_DIR_VAR).unwrap_or(String::from(STATES_DIR_DEFAULT));
        FilesystemStateManager::new(basedir).unwrap()
    };
}

#[derive(Debug, Deserialize)]
pub struct QueryParams {
    #[serde(rename = "ID")]
    lock_id: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let address: SocketAddr = env::var(ADDRESS_VAR)
        .unwrap_or(String::from(ADDRESS_DEFAULT))
        .parse()
        .unwrap();

    pretty_env_logger::init();

    let base = warp::path::param::<String>();

    let read = base.and(warp::get()).map(|name: String| {
        let res = STATES.fetch(name);

        match res {
            Ok(state) => state.into_response(),
            Err(err) => err.into_response(),
        }
    });

    let write = base
        .and(warp::post())
        .and(warp::query())
        .and(warp::body::bytes())
        .map(|name: String, query: QueryParams, body: Bytes| {
            let res = match query.lock_id {
                Some(lock_id) => STATES.update_locked(name, body, lock_id),
                None => STATES.update(name, body),
            };

            match res {
                Ok(_) => Response::builder().status(200).body(Body::empty()).unwrap(),
                Err(err) => err.into_response(),
            }
        });

    let delete = base.and(warp::delete()).map(|name: String| {
        let res = STATES.purge(name);

        match res {
            Ok(_) => Response::builder().status(200).body(Body::empty()).unwrap(),
            Err(err) => err.into_response(),
        }
    });

    let lock = base.and(filters::lock()).and(warp::body::json()).map(
        |name: String, _, lock_info: LockInfo| {
            let res = STATES.lock(name, lock_info);

            match res {
                Ok(data) => warp::reply::json(&data).into_response(),
                Err(err) => err.into_response(),
            }
        },
    );

    let unlock = base.and(filters::unlock()).and(warp::body::json()).map(
        |name: String, _, lock_info: LockInfo| {
            let res = STATES.unlock(name, lock_info);

            match res {
                Ok(_) => Response::builder().status(200).body(Body::empty()).unwrap(),
                Err(err) => err.into_response(),
            }
        },
    );

    let routes = read
        .or(write)
        .or(delete)
        .or(lock)
        .or(unlock)
        .with(warp::log("backend:server"));

    warp::serve(routes).run(address).await;

    Ok(())
}
