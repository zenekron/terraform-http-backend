# build
FROM rust:1.47.0 as build
RUN rustup target add x86_64-unknown-linux-musl
WORKDIR /app
COPY . .
RUN cargo build --release --target x86_64-unknown-linux-musl


# runtime
FROM alpine:latest

ENV BACKEND_ADDRESS=0.0.0.0:10000
ENV BACKEND_STATES_DIR=./states

WORKDIR /app
COPY --from=build /app/target/x86_64-unknown-linux-musl/release/terraform-http-backend terraform-http-backend

CMD ["./terraform-http-backend"]
